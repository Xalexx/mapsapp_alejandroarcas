package com.example.mapsapp_alejandroarcas.model

data class Marker(
    var email: String?=null,
    val markerTitle: String?=null,
    val markerLat: String?=null,
    val markerLang: String?=null)