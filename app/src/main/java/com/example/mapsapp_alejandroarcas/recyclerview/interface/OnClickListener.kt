package com.example.mapsapp_alejandroarcas.recyclerview.`interface`

import com.example.mapsapp_alejandroarcas.model.Marker

interface OnClickListener {
    fun onClickDelete(marker: Marker)
}