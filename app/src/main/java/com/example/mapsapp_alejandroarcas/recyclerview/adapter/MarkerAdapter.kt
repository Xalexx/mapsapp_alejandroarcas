package com.example.mapsapp_alejandroarcas.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mapsapp_alejandroarcas.R
import com.example.mapsapp_alejandroarcas.databinding.MarkerItemBinding
import com.example.mapsapp_alejandroarcas.model.Marker
import com.example.mapsapp_alejandroarcas.recyclerview.`interface`.OnClickListener

class MarkerAdapter(private var markers: MutableList<Marker>,
                    private var listener: OnClickListener): RecyclerView.Adapter<MarkerAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = MarkerItemBinding.bind(view)

        fun setListener(marker: Marker){
            binding.trashButtonButton.setOnClickListener {
                listener.onClickDelete(marker)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.marker_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marker = markers[position]
        with(holder){
            setListener(marker)
            binding.markerNameTextView.text = marker.markerTitle
            binding.emailTextView.text = marker.email
            binding.latitudeTextView.text = marker.markerLat
            binding.longitudeTextView.text = marker.markerLang
        }
    }

    override fun getItemCount(): Int {
        return markers.size
    }

    /*
    fun delete(marker: Marker) {
        val index = markers.indexOf(marker)
        if(index != -1){
            markers.removeAt(index)
            notifyItemRemoved(index)
            notifyDataSetChanged()
        }
    } */

    fun setMarkerList(markerList: MutableList<Marker>){
        markers = markerList
        notifyDataSetChanged()
    }

}