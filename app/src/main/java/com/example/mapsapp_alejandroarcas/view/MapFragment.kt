package com.example.mapsapp_alejandroarcas.view

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.mapsapp_alejandroarcas.R
import com.example.mapsapp_alejandroarcas.databinding.FragmentMapBinding
import com.example.mapsapp_alejandroarcas.viewmodel.MarkersViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import org.w3c.dom.Text

const val REQUEST_CODE_LOCATION = 100

class MapFragment : Fragment(), OnMapReadyCallback, OnMapLongClickListener {

    private lateinit var map: GoogleMap
    private lateinit var binding: FragmentMapBinding
    private val db = FirebaseFirestore.getInstance()
    private var userEmail = ""
    private var markerName = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Call mapCreator
        binding = FragmentMapBinding.inflate(inflater,container, false)
        userEmail = arguments?.getString("userEmail").toString()
        createMap()
        return binding.root
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        createMarker()
        map.setOnMapLongClickListener(this)
        enableLocation()
    }

    private fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun createMarker(){
        val coordinates = LatLng(41.453468990844605, 2.18629915521161)
        val myMarker = MarkerOptions().position(coordinates).title("ITB")
        map.addMarker(myMarker)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
            5000, null)
    }


    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        requireContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("SetTextI18n", "MissingInflatedId")
    override fun onMapLongClick(coordinates: LatLng) {
        var dialog: AlertDialog? = null
        val builder = AlertDialog.Builder(context)

        val view = layoutInflater.inflate(R.layout.layout_dialog, null)
        val markerTitle: EditText = view.findViewById(R.id.tv_edittext)
        val tvTitle: TextView = view.findViewById(R.id.tv_title)
        val tvInvalid: TextView = view.findViewById(R.id.tv_invalid)
        val tvInvalidImg: ImageView = view.findViewById(R.id.danger)
        val btDone: Button = view.findViewById(R.id.bt_done)

        tvTitle.text = "Marker Configuration"
        markerTitle.hint = "Enter a marker name"
        markerTitle.setTextColor(Color.WHITE)
        markerTitle.setHintTextColor(Color.WHITE)

        btDone.setOnClickListener {
            markerName = markerTitle.text.toString()
            println(markerName)
            if (markerName.isNotEmpty()){
                tvInvalidImg.visibility = View.GONE
                // Push marker to firebase database
                pushMarkerTitleToDatabase(markerTitle.text.toString(), coordinates.latitude.toString(), coordinates.longitude.toString())
                val myMarker = MarkerOptions().position(coordinates).title(markerName)
                map.addMarker(myMarker)
                dialog?.dismiss()
                val viewModel = ViewModelProvider(requireActivity())[MarkersViewModel::class.java]
                viewModel.email = userEmail
            } else {
                tvInvalidImg.visibility = View.VISIBLE
                tvInvalid.text = "Invalid Marker Title"
            }
        }

        builder.setView(view)
        dialog = builder.create()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    private fun pushMarkerTitleToDatabase(title: String, lat: String, lang: String) {
        db.collection("users").document(userEmail).collection("markers").document(title).set(
            hashMapOf("markerTitle" to title,
            "latitude" to lat,
            "longitude" to lang))
        }
}