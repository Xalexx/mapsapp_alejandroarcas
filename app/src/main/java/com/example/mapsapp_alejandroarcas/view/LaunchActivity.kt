package com.example.mapsapp_alejandroarcas.view

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.example.mapsapp_alejandroarcas.R

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        supportActionBar?.hide() // Remove ActionBar
        val slideAnimation = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_left_to_right)
        val firstCloud = findViewById<ImageView>(R.id.cloud1)
        val secondCloud = findViewById<ImageView>(R.id.cloud2)
        val thirdCloud = findViewById<ImageView>(R.id.cloud3)
        val fourFCloud = findViewById<ImageView>(R.id.cloud4)

        firstCloud.startAnimation(slideAnimation)
        secondCloud.startAnimation(slideAnimation)
        thirdCloud.startAnimation(slideAnimation)
        fourFCloud.startAnimation(slideAnimation)

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            Animatoo.animateFade(this)
        }, 3500)
    }
}