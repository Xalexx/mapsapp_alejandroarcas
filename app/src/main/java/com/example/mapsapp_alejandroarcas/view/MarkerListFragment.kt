package com.example.mapsapp_alejandroarcas.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mapsapp_alejandroarcas.databinding.FragmentMarkerListBinding
import com.example.mapsapp_alejandroarcas.model.Marker
import com.example.mapsapp_alejandroarcas.recyclerview.adapter.MarkerAdapter
import com.example.mapsapp_alejandroarcas.recyclerview.`interface`.OnClickListener
import com.example.mapsapp_alejandroarcas.viewmodel.MarkersViewModel
import com.google.firebase.firestore.*

class MarkerListFragment : Fragment(), OnClickListener {

    private lateinit var binding: FragmentMarkerListBinding
    private lateinit var markerAdapter: MarkerAdapter
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var markerList: ArrayList<Marker>
    private val db = FirebaseFirestore.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentMarkerListBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            setUpRecyclerView(arrayListOf())
            markerList = arrayListOf<Marker>()
            eventChangeListener()
        }

    override fun onClickDelete(marker: Marker) {
        TODO("Not yet implemented")
    }


    private fun setUpRecyclerView(list: MutableList<Marker>) {
        markerAdapter = MarkerAdapter(list, this)
        mLayoutManager = LinearLayoutManager(context)

        binding.recyclerMarker.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = markerAdapter
        }
    }

    private fun eventChangeListener() {
        val viewModel = ViewModelProvider(requireActivity())[MarkersViewModel::class.java]
        db.collection("users").document(viewModel.email).collection("markers").addSnapshotListener(object: EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if (error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for (dc: DocumentChange in value?.documentChanges!!){
                    if (dc.type == DocumentChange.Type.ADDED){
                        val newMarker = dc.document.toObject(Marker::class.java)
                        newMarker.email = dc.document.id
                        markerList.add(newMarker)
                    }
                }
                markerAdapter.setMarkerList(markerList)
            }
        })
    }
}