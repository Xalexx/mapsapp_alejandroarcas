package com.example.mapsapp_alejandroarcas.view

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.mapsapp_alejandroarcas.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var userEmail: String
        var userPassword: String

        binding.loginButton.setOnClickListener {

            userEmail = binding.emailEditText.text.toString()
            userPassword = binding.passwordEditText.text.toString()

            if (userEmail == "" || userPassword == "") showError("El campo email o contraseña no pueden estar vacios")
            else if (!userEmail.contains('@')) showError("Escribe un email válido")
            else {
                FirebaseAuth.getInstance().
                signInWithEmailAndPassword(userEmail, userPassword)
                    .addOnCompleteListener {
                        if (it.isSuccessful){
                            println("hola")
                            println("hola")
                            println(userEmail)
                            println("hola")
                            println("hola")
                            val action = LoginFragmentDirections.actionLoginFragmentToMapFragment(userEmail)
                            findNavController().navigate(action)
                        } else showError("Email o contraseña incorrectos")
                    }
            }
        }

        binding.registerButton.setOnClickListener{

            userEmail = binding.emailEditText.text.toString()
            userPassword = binding.passwordEditText.text.toString()

            if (userEmail == "" || userPassword == "") showError("El campo email o contraseña no pueden estar vacios")
            else if (!userEmail.contains('@')) showError("Escribe un email válido")
            else {
                FirebaseAuth.getInstance().
                createUserWithEmailAndPassword(userEmail, userPassword)
                    .addOnCompleteListener {
                        if (it.isSuccessful){
                            val action = LoginFragmentDirections.actionLoginFragmentToMapFragment(userEmail)
                            findNavController().navigate(action)
                        } else showError("Error al hacer registro")
                    }
            }
        }

    }

    private fun showError(s: String) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show()
    }
/*
    private fun goToHome(emailLogged: String) {

    }*/
}